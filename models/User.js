const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First Name is required']
    },
    lastName: {
        type: String,
        require: [true, 'Last Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required.']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, 'Mobile # is required.']
    },
    enrollments: [{
        courseId: {
            type: String,
            required: [true, 'Course ID is require']
        },
        enrolledOn: {
            type: Date,
            default: new Date()
        },
        status: {
            type: String,
            default: 'enrolled'
        }
    }]
})

module.exports = mongoose.model('Users', userSchema)
