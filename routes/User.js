const express = require("express");
const router = express.Router();
const userController = require('../Controllers/user-controller')
const auth = require('../auth')

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post('/details', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile(req.body.id).then(resultFromController => res.send(resultFromController))
})

/* for enrolling a user */
router.post('/enroll', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: req.body.userId,
        courseId: req.body.courseId
    }

    if (userData.isAdmin == true) {
        res.send(`User Not Authorized.`)
    } else {
        userController.enroll(data).then(resultFromController => res.send(resultFromController));
    }
})

module.exports = router;