const course = require('../models/Course');

module.exports.addCourse = (reqBody) => {
    let newCourse = new course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })

    return newCourse.save().then((success, err) => {
        if (err) {
            return false
        } else {
            return true
        }
    })
}

/* retreive all courses */
module.exports.getAllCourses = () => {
    return course.find({}).then(result => {
        return result;
    })
}

/* retrieve active courses */
module.exports.getActiveCourses = () => {
    return course.find({ isActive: true }).then(result => {
        return result;
    })
}

/* retrieve specific course */
module.exports.getCourse = (reqParams) => {
    return course.findById(reqParams.courseId).then(result => {
        return (`name:${result.name}`);
    })
}

/* update a course */
module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        }
        // Syntax: findByIdAndUpdate(document id,updatesToBeApplied)
    return course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if (error) {
            return false
        } else {
            return `updated course`
        }
    })
}

/* archive method */
module.exports.archiveCourse = (reqParams, reqBody) => {
    return course.findByIdAndUpdate(reqParams.courseId).then(result => {
        if (result == null) {
            return false
        } else {
            result.isActive = reqBody.isActive
            return result.save().then((success, saveErr) => {
                if (saveErr) {
                    console.log(saveErr)
                    return false
                } else {
                    return success
                }
            })
        }
    })

}